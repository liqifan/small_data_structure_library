import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BST<T extends Comparable<T>> {

    private class Node{

        public T t;
        public Node left,right;

        public Node(T t){
            this.t = t;
            left = null;
            right = null;
        }
    }

    private Node root;
    private int size;

    public BST(){
        root = null;
        size = 0;
    }

    public int size(){
        return size;
    }

    public boolean isEmpty(){
        return size==0;
    }

    /**
     * 向二分搜索树中添加新的元素t
     */
    public void add(T t){
//        if(root == null){
//            root = new Node(t);
//            size ++;
//        }else {
//            add(root,t);
//        }
        root = add(root,t);
    }

    /**
     * 向以node为根的二分搜索树中插入元素t，递归算法
     */
    private Node add(Node node,T t){
        //精简版：
        if(node == null){
            size ++;
            return new Node(t);
        }
        //若左孩子或右孩子不为null
        if(t.compareTo(node.t) < 0){
            node.left = add(node.left,t);
        }else if(t.compareTo(node.t)>0){
            node.right = add(node.right,t);
        }

        return node;
    }
//    private void add(Node node,T t){
//        if(t.equals(node.t)){
//            return;
//        }else if(t.compareTo(node.t) < 0 && node.left == null){
//            node.left = new Node(t);
//            size ++;
//            return;
//        }else if(t.compareTo(node.t) > 0 && node.right == null){
//            node.right = new Node(t);
//            size ++;
//            return;
//        }
//        //若左孩子或右孩子不为null
//        if(t.compareTo(node.t) < 0){
//            add(node.left,t);
//        }else if(t.compareTo(node.t)>0){
//            add(node.right,t);
//        }
//    }


    /**
     * 看二分搜索树中是否包含元素t
     */
    public boolean contains(T t){
        return contains(root,t);
    }

    private boolean contains(Node node,T t){
        if(node == null){
            return false;
        }

        if(t.equals(node.t)){
            return true;
        }else if(t.compareTo(node.t)<0){
            return contains(node.left,t);
        }else {
            return contains(node.right,t);
        }
    }

    //二分搜索树的前序遍历
    public void preOrder(){
        preOrder(root);
    }

    //前序遍历以node为根的二分搜索树，递归算法
    private void preOrder(Node node){
        if(node == null){
            return;
        }
        System.out.println(node.t);
        preOrder(node.left);
        preOrder(node.right);
    }

    //前序遍历以node为根的二分搜索树，非递归算法（深度优先算法）
    public void preOrderNR(){

        Stack<Node> stack = new Stack<>();
        //先将根节点入栈
        stack.push(root);
        while (!stack.isEmpty()){
            Node pop = stack.pop();
            System.out.println(pop.t);

            if(pop.right != null){
                stack.push(pop.right);
            }
            if(pop.left != null){
                stack.push(pop.left);
            }
        }

    }

    //中序遍历
    public void inOrder(){
        inOrder(root);
    }
    //中序遍历以node为根的二分搜索树，递归算法
    private void inOrder(Node node){

        if(node == null){
            return;
        }
        inOrder(node.left);
        System.out.println(node.t);
        inOrder(node.right);
    }

    //中序遍历以node为根的二分搜索树，非递归算法
    public void inOrderNR(){
        Stack<Node> stack = new Stack<>();

    }


    //后序遍历
    public void postOrder(){
        postOrder(root);
    }
    //中序遍历以node为根的二分搜索树，递归算法
    private void postOrder(Node node){

        if(node == null){
            return;
        }
        postOrder(node.left);
        postOrder(node.right);
        System.out.println(node.t);
    }
    //中序遍历以node为根的二分搜索树，非递归算法
    public void postOrderNR(){
        Stack<Node> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()){
            Node cur = stack.peek();
            if(cur.left != null){
                stack.push(cur.left);
            }
            if(cur.left == null && cur.right == null){
                System.out.println(cur.t);
                stack.pop();
            }

        }
    }

    //层序遍历（广度优先遍历）
    public void levelOrder(){

        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()){
            Node cur = queue.remove();
            System.out.println(cur.t);

            if(cur.left != null){
                queue.add(cur.left);
            }
            if(cur.right != null){
                queue.add(cur.right);
            }
        }

    }

    //寻找二分搜索树的最小元素
    public T minimum(){
        if(size == 0){
            throw new IllegalArgumentException("BST is empty");
        }

        Node minNode = minimum(root);
        return minNode.t;
    }

    //返回以node为根的二分搜索树的最小值所在的节点
    private Node minimum(Node node){
        if(node.left == null){
            return node;
        }

        return minimum(node.left);
    }

    //寻找二分搜索树的最大元素
    public T maximum(){
        if(size == 0){
            throw new IllegalArgumentException("BST is empty");
        }

        Node maxNode = maximum(root);
        return maxNode.t;
    }

    //返回以node为根的二分搜索树的最大值所在的节点
    private Node maximum(Node node){
        if(node.right == null){
            return node;
        }

        return maximum(node.right);
    }

    //从二分搜索树中删除最小值所在的节点，返回最小值
    public T removeMin(){
        T ret = minimum();
        root = removeMin(root);
        return  ret;
    }

    //删除掉以node为根的二分搜索树中的最小节点
    //返回删除节点后新的二分搜索树的根
    private Node removeMin(Node node){
        if(node.left == null){
            Node rightNode = node.right;
            node.right = null;
            size --;
            return rightNode;
        }

        node.left = removeMin(node.left);
        return node;
    }

    //从二分搜索树中删除最大值所在的节点
    public T removeMax(){
        T ret = maximum();
        root = removeMax(root);

        return ret;
    }

    //删除掉以node为根的二分搜索树中的最大节点
    //返回删除节点后新的二分搜索树的根
    private Node removeMax(Node node){
        if(node.right == null){
            Node leftNode = node.left;
            node.left = null;
            size --;
            return leftNode;
        }

        node.right = removeMax(node.right);

        return node;
    }

    //从二分搜索树中删除元素为t的节点
    public void remove(T t){
        root = remove(root,t);
    }

    //删除以node为根的二分搜索树中值为t的节点，递归算法
    //返回删除节点后新的二分搜索树的根
    private Node remove(Node node , T t){
        //最基本的情况：没找到值为t的节点
        if(node == null){
            return null;
        }

        if(t.compareTo(node.t) < 0){
            node.left = remove(node.left, t);
            return node;
        }else if(t.compareTo(node.t) > 0){
            node.right = remove(node.right,t);
            return node;
        }else{
            if(node.left == null){
                Node rightNode = node.right;
                node.right = null;
                size --;
                return rightNode;
            }
            if(node.right == null){
                Node leftNode = node.left;
                node.left = null;
                size --;
                return leftNode;
            }

            Node successor = minimum(node.right);
            successor.right = removeMin(node.right);
            successor.left = node.left;

            node.left = node.right = null;
            return successor;
        }
    }

    @Override
    public String toString(){
        StringBuilder res = new StringBuilder();
        generaBSTString(root,0,res);
        return res.toString();
    }

    //生成以node为根节点，深度为depth的描述二叉树的字符串
    private void generaBSTString(Node node,int depth,StringBuilder res){

        if(node == null){
			res.append(generateDepthString(depth) + "null\n");
            return;
        }

        res.append(generateDepthString(depth)+node.t+"\n");
        generaBSTString(node.left,depth+1,res);
        generaBSTString(node.right,depth+1,res);
    }

    private String generateDepthString(int depth){
        StringBuilder res = new StringBuilder();
        for(int i = 0;i<depth;i++){
            res.append("--");
        }
        return res.toString();
    }
}
