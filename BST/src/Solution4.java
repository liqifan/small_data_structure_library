import java.util.Stack;

/*
给你一份『词汇表』（字符串数组） words 和一张『字母表』（字符串） chars。

假如你可以用 chars 中的『字母』（字符）拼写出 words 中的某个『单词』（字符串），那么我们就认为你掌握了这个单词。

注意：每次拼写时，chars 中的每个字母都只能用一次。

返回词汇表 words 中你掌握的所有单词的 长度之和。
 */

class Solution4 {

    public static int countCharacters(String[] words, String chars) {
        int size = 0;
        Stack<Character> stack = new Stack<>();
        for(int i=0;i<chars.length();i++){
            stack.push(chars.charAt(i));
        }

        for(int i=0;i<words.length;i++){
            int count = 0;
            Stack<Character> s = (Stack<Character>)stack.clone();

//            System.out.println(words[i]);
            for(int j=0;j<words[i].length();j++){
                char parm = words[i].charAt(j);
                if(s.contains(parm)){
                    s.removeElement(parm);
                    System.out.println(s);
                    count ++;
                    continue;
                }else {
                    count =0;
                    break;
                }
            }
            size += count;
        }
        System.out.println(stack);
        return size;
    }

    public static void main(String[] args) {
        String[] words = {"cat","bt","hat","tree"};
        String chars = "atach";
        int res = countCharacters(words, chars);
        System.out.println(res);
    }
}