import java.util.ArrayList;
import java.util.List;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */

/**
 * 二叉树的中序遍历，递归算法
 */
class Solution3 {

     public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }

    public List<Integer> inorderTraversal(TreeNode root) {
         List<Integer> res = new ArrayList<>();
         if(root == null || root.equals("")){
             return res;
         }
         res = inOrder(root,res);
         return res;
    }

    private List<Integer> inOrder(TreeNode root,List<Integer> res) {
        if(root == null){
            return null;
        }

        inOrder(root.left,res);
        res.add(root.val);
        inOrder(root.right,res);

        return res;
    }

}