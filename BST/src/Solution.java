import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.LinkedList;
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */

/*
给定一个二叉树，返回其按层次遍历的节点值。 （即逐层地，从左到右访问所有节点）。

例如:
给定二叉树: [3,9,20,null,null,15,7],

    3
   / \
  9  20
    /  \
   15   7
返回其层次遍历结果：

[
  [3],
  [9,20],
  [15,7]
]
 */

/**
 * Queue 中 remove() 和 poll()都是用来从队列头部删除一个元素。
 * 在队列元素为空的情况下，
 * remove() 方法会抛出NoSuchElementException异常，
 * poll() 方法只会返回 null 。
 */
class Solution {

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }

    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList();

        if(root == null){
            return res;
        }
        queue.add(root);
        while(!queue.isEmpty()){
            int count = queue.size();
            List<Integer> p = new ArrayList<>();
            while (count > 0){
                TreeNode cur = queue.poll();
                p.add(cur.val);

                if(cur.left != null){
                    queue.add(cur.left);
                }
                if(cur.right != null){
                    queue.add(cur.right);
                }
                count --;
            }
            res.add(p);
        }
        return res;
    }
}