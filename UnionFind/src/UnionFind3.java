/*
        第三版并查集
         基于size进行的优化
 */
public class UnionFind3 implements UF {

    private int[] parent;
    private int[] size;  //size[i]表示以i为根的集合中元素的个数

    public UnionFind3(int size){
        parent = new int[size];
        this.size = new int[size];

        for(int i:parent){
            parent[i] = i;
            this.size[i] = 1;
        }
    }

    @Override
    public int getSize() {
        return parent.length;
    }

    //寻找根节点
    private int find(int p){
        if(p<0 || p>= parent.length){
            throw new IllegalArgumentException("p is Illegal");
        }

        while (p != parent[p]){
            //寻找根节点
            p = parent[p];
        }
        return p;
    }

    //查询元素p和q是否在同一集合
    //O(h)时间复杂度 h为树的高度
    @Override
    public boolean isConnected(int p, int q) {
        return find(p) == find(q);
    }

    //合并元素p和q所在的集合
    //O(h)时间复杂度 h为树的高度
    @Override
    public void unionElements(int p, int q) {
        int pRoot = find(p);
        int qRoot = find(q);
        if(pRoot == qRoot){
            return;
        }

        if(size[pRoot] < size[qRoot]){
            parent[pRoot] = qRoot;
            size[qRoot] += size[pRoot];
        }else {
            parent[qRoot] = pRoot;
            size[pRoot] += size[qRoot];
        }

    }
}
