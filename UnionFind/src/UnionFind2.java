//第二版并查集   特殊的树结构(孩子节点指向父节点)
public class UnionFind2 implements UF {

    private int[] parent;

    public UnionFind2(int size){
        parent = new int[size];

        for(int i:parent){
            parent[i] = i;
        }
    }

    @Override
    public int getSize() {
        return parent.length;
    }

    //寻找根节点
    private int find(int p){
        if(p<0 || p>= parent.length){
            throw new IllegalArgumentException("p is Illegal");
        }

        while (p != parent[p]){
            //寻找根节点
            p = parent[p];
        }
        return p;
    }

    //查询元素p和q是否在同一集合
    //O(h)时间复杂度 h为树的高度
    @Override
    public boolean isConnected(int p, int q) {
        return find(p) == find(q);
    }

    //合并元素p和q所在的集合
    //O(h)时间复杂度 h为树的高度
    @Override
    public void unionElements(int p, int q) {
        int pRoot = find(p);
        int qRoot = find(q);
        if(pRoot == qRoot){
            return;
        }
        parent[pRoot] = qRoot;
    }
}
