/**
 * 设计一个支持 push，pop，top 操作，并能在常数时间内检索到最小元素的栈。
 *
 * push(x) -- 将元素 x 推入栈中。
 * pop() -- 删除栈顶的元素。
 * top() -- 获取栈顶元素。
 * getMin() -- 检索栈中的最小元素。
 */
import java.util.Stack;

class MinStack {

    private Stack<Integer> stack;
    private Stack<Integer> stackMin;
    private int size;

    /** initialize your data structure here. */
    public MinStack() {
        stack = new Stack<>();
        stackMin = new Stack<>();
        size = 0;
    }
    
    public void push(int x) {
        stack.push(x);
        if(stackMin.isEmpty()){
            stackMin.push(x);
        }else {
            if(stackMin.peek()>=x){
                stackMin.push(x);
            }
        }
        size ++;
    }
    
    public void pop() {
        Integer pop = stack.pop();
        if(pop.equals(stackMin.peek())){
            stackMin.pop();
        }
        size --;
    }
    
    public int top() {
        return stack.peek();
    }
    
    public int getMin() {
        int min = stackMin.peek();
        return min;
    }

    public static void main(String[] args) {
        MinStack minStack = new MinStack();

        minStack.push(512);
        minStack.push(-1024);
        minStack.push(-1024);
        minStack.push(512);

        minStack.pop();
        System.out.println(minStack.getMin());
        minStack.pop();
        System.out.println(minStack.getMin());
        minStack.pop();
        System.out.println(minStack.getMin());
    }

}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */