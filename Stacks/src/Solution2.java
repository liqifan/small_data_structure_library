import java.util.Stack;

/**
 * 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串，判断字符串是否有效。
 *
 * 有效字符串需满足：
 *
 * 左括号必须用相同类型的右括号闭合。
 * 左括号必须以正确的顺序闭合。
 * 注意空字符串可被认为是有效字符串。
 *
 * 输入: "()"
 * 输出: true
 * 示例 2:
 *
 * 输入: "()[]{}"
 * 输出: true
 * 示例 3:
 *
 * 输入: "(]"
 * 输出: false
 */
class Solution2 {
    public boolean isValid(String s) {

        Stack<Character> stack = new Stack<>();
        for(int i=0;i<s.length();i++){
            char r = s.charAt(i);
            if('(' == r || '[' == r || '{' == r){
                stack.push(r);
            }else {
                if(stack.isEmpty()){
                    return false;
                }
                char top = stack.pop();
                if(')' == r && top != '('){
                    return false;
                }
                if(']' == r && top != '['){
                    return false;
                }
                if('}' == r && top != '{'){
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}