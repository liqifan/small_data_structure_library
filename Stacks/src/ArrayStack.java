/**
 * 自定义栈
 *      是在自定义数组类的基础上
 * @param <T>
 */
public class ArrayStack<T> implements Stack<T> {

    private Array<T> array;

    public ArrayStack(int capacity){
        this.array = new Array<>(capacity);
    }

    public ArrayStack(){
        this.array = new Array<>();
    }

    @Override
    public int getSize() {
        return array.getSize();
    }

    @Override
    public boolean isEmpty() {
        return array.isEmpty();
    }

    @Override
    public void push(T t) {
        array.addLast(t);
    }

    @Override
    public T pop() {
        return array.removeLast();
    }

    @Override
    public T peek() {
        return array.getLast();
    }

    //获取栈的容量
    public  int getCapacity(){
        return array.getCapacity();
    }

    @Override
    public String toString(){
        StringBuilder res = new StringBuilder();
        res.append("Stack: ");
        res.append("[");
        for(int i = 0;i<array.getSize();i++){
            res.append(array.get(i));
            if( i != array.getSize()-1){
                res.append(", ");
            }
        }
        res.append("] top");
        return res.toString();
    }
}
