public interface Stack <T>{

    int getSize();
    boolean isEmpty();
    void push(T t);
    T pop();
    //查看栈顶元素
    T peek();
}
