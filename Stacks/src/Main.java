import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        Scanner sc2 = new Scanner(System.in);
        String[] parmList = sc2.nextLine().split(" ");
        List<Integer> parm= new ArrayList<>();

        Stack<Integer> minStack = new Stack<>();
        Stack<Integer> maxStack = new Stack<>();
        Stack<Integer> parmStack = new Stack<>();

        for(int i=0;i<n;i++){
            int r = Integer.parseInt(parmList[i]);
            parm.add(Integer.parseInt(parmList[i]));
            parmStack.push(r);
            if(minStack.size()==0){
                minStack.push(r);
            }else {
                if(minStack.peek()>r){
                    minStack.push(r);
                }
            }
            if(maxStack.size()==0){
                maxStack.push(r);
            }else {
                if(maxStack.peek() < r){
                    maxStack.push(r);
                }
            }
        }
        double res;
        double bigDecimal;
        int i ;
        if(n%2 == 0){
            res = (parmStack.get(n/2)+parmStack.get(n/2 - 1))/2.0;
            if(res == (int)res){
                i = (int)res;
                System.out.print(maxStack.peek()+" "+i+" "+minStack.peek());
                return;
            }else {
                bigDecimal = new BigDecimal(res).setScale(0, BigDecimal.ROUND_HALF_UP).doubleValue();
                System.out.print(maxStack.peek()+" "+bigDecimal+" "+minStack.peek());
                return;
            }
        }else {
            i = new BigDecimal(parmStack.get(n/2)).intValue();
            System.out.print(maxStack.peek()+" "+i+" "+minStack.peek());
            return;
        }
    }
}
