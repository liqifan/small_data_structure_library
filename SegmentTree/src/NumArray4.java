class NumArray4 {

    private int[] sum; // sum[i]存储前i个元素和，sum[0] =0
                        // sum[i]存储num[0....i-1]的和
    private int[] data;

    public NumArray4(int[] nums) {
        if(nums.length>0){
            data = new int[nums.length];
            for(int i=0;i<nums.length;i++){
                data[i] = nums[i];
            }
            sum = new int[nums.length + 1];
            sum[0] = 0;
            for(int i=1;i<sum.length;i++){
                sum[i] = sum[i-1] + nums[i-1];
            }
        }
    }

    public void update(int index, int val) {
        data[index] = val;
        for(int i=index + 1;i<sum.length;i++){
            sum[i] = sum[i-1] + data[i-1];
        }
    }

    public int sumRange(int i, int j) {
        return sum[j + 1] - sum[i];
    }
}

/**
 * Your NumArray4 object will be instantiated and called as such:
 * NumArray4 obj = new NumArray4(nums);
 * obj.update(i,val);
 * int param_2 = obj.sumRange(i,j);
 */