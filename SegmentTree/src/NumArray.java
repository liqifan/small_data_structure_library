class NumArray {

    private SegmentTree<Integer> segmentTree;

    public NumArray(int[] nums) {
        if(nums.length > 0){
            Integer[] res = new Integer[nums.length];
            for(int i=0;i<nums.length;i++){
                res[i] = nums[i];
            }
            segmentTree = new SegmentTree<Integer>(res,(a,b)->a+b);
        }
    }

    public void update(int index, int val) {
        segmentTree.set(index,val);
    }

    public int sumRange(int i, int j) {
        return segmentTree.query(i,j);
    }
}

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray obj = new NumArray(nums);
 * obj.update(i,val);
 * int param_2 = obj.sumRange(i,j);
 */