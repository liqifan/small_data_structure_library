class NumArray2 {

    private SegmentTree<Integer> segmentTree;

    public NumArray2(int[] nums) {
        if(nums.length > 0){
            Integer[] res = new Integer[nums.length];
            for(int i=0;i<nums.length;i++){
                res[i] = nums[i];
            }
            segmentTree = new SegmentTree<Integer>(res,(a,b)->a+b);
        }
    }
    
    public int sumRange(int i, int j) {
        return segmentTree.query(i,j);
    }
}

/**
 * Your NumArray2 object will be instantiated and called as such:
 * NumArray2 obj = new NumArray2(nums);
 * int param_1 = obj.sumRange(i,j);
 */