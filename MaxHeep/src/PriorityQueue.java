/**
 * 基于 最大堆 实现 优先队列
 *      最大值优先级就最高
 */
public class PriorityQueue<T extends Comparable<T>> implements Queue<T> {

    private MaxHeep<T> maxHeep;

    public PriorityQueue(){
        maxHeep = new MaxHeep<>();
    }

    @Override
    public int getSize() {
        return maxHeep.size();
    }

    @Override
    public boolean isEmpty() {
        return maxHeep.isEmpty();
    }

    @Override
    public void enqueue(T t) {
        maxHeep.add(t);
    }

    @Override
    public T dequeue() {
        return maxHeep.extractMax();
    }

    @Override
    public T getFront() {
        return maxHeep.findMax();
    }
}
