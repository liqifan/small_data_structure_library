/*
给定两个数组，编写一个函数来计算它们的交集。

示例 1:

输入: nums1 = [1,2,2,1], nums2 = [2,2]
输出: [2,2]
示例 2:

输入: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
输出: [4,9]
说明：

输出结果中每个元素出现的次数，应与元素在两个数组中出现的次数一致。
我们可以不考虑输出结果的顺序。
 */
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

class Solution {
    public static int[] intersection(int[] nums1, int[] nums2) {
        TreeMap<Integer,Integer> map = new TreeMap<>();

        for(int num:nums1){
            if(!map.containsKey(num)){
                map.put(num,1);
            }else {
                map.put(num,map.get(num) + 1);
            }
        }

        List<Integer> list = new ArrayList<>();
        for(int num:nums2){
            if(map.containsKey(num)){
                list.add(num);
                map.put(num,map.get(num) - 1);
                if(map.get(num) == 0){
                    map.remove(num);
                }
            }
        }
        int[] res = new int[list.size()];
        for(int i=0;i<list.size();i++){
            res[i] = list.get(i);
            System.out.println(res[i]);
        }

        return res;
    }

    public static void main(String[] args) {
        int[] num1= {4,9,5};
        int[] num2 = {9,4,9,8,4};
        intersection(num1,num2);
    }
}