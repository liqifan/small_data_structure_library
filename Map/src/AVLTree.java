import java.util.ArrayList;

public class AVLTree<K extends Comparable<K>,V>{

    private class Node{

        public K key;
        public V value;
        public Node left;
        public Node right;
        public int height;

        public Node(K key,V value){
            this.key = key;
            this.value = value;
            left = null;
            right = null;
            height = 1;
        }
    }

    private int size;
    private Node root;

    public AVLTree(){
        root = null;
        size = 0;
    }

    //返回以node为根节点的二分搜索树中，key所在的节点
    private Node getNode(Node node,K key){
        if(node == null){
            return null;
        }

        if(key.compareTo(node.key) == 0){
            return node;
        }else if(key.compareTo(node.key) < 0){
            return getNode(node.left,key);
        }else { //key.compareTo(node.key) > 0
            return getNode(node.right,key);
        }
    }

    //返回节点的高度
    private int getHeight(Node node){
        if(node == null){
            return 0;
        }
        return node.height;
    }

    //计算平衡因子
    private int getBalanceFactor(Node node){
        if(node == null){
            return 0;
        }
        return getHeight(node.left) - getHeight(node.right);
    }

    /**
     * 右旋转
     */
    // 对节点y进行向右旋转操作，返回旋转后新的根节点x
    //        y                              x
    //       / \                           /   \
    //      x   T4     向右旋转 (y)        z     y
    //     / \       - - - - - - - ->    / \   / \
    //    z   T3                       T1  T2 T3 T4
    //   / \
    // T1   T2
    private Node rightRotate(Node y){
        Node x = y.left;
        Node T3 = x.right;

        //右旋转
        x.right = y;
        y.left = T3;

        //维护height
        y.height = 1 + Math.max(getHeight(y.left),getHeight(y.right));
        x.height = 1 + Math.max(getHeight(x.left),getHeight(x.right));

        return x;
    }

    /**
     * 左旋转
     */
    // 对节点y进行向右左转操作，返回旋转后新的根节点x
    //        y                              x
    //       / \                           /   \
    //      T4  x     向右旋转 (y)          y    z
    //         / \     - - - - - - - ->   / \  / \
    //        T3  z                     T3  T4 T1 T2
    //           / \
    //         T1   T2
    private Node leftRotate(Node y){
        Node x = y.right;
        Node T3 = x.left;

        //左旋转
        x.left = y;
        y.right = T3;

        //维护height
        y.height = 1 + Math.max(getHeight(y.left),getHeight(y.right));
        x.height = 1 + Math.max(getHeight(x.left),getHeight(x.right));

        return x;
    }
    public void add(K key, V value) {
        root = add(root,key,value);
    }

    /**
     * 向以node为根的二分搜索树中插入元素(K,V)，递归算法
     */
    private Node add(Node node,K key,V value){
        //精简版：
        if(node == null){
            size ++;
            return new Node(key,value);
        }
        //若左孩子或右孩子不为null
        if(key.compareTo(node.key) < 0){
            node.left = add(node.left,key,value);
        }else if(key.compareTo(node.key)>0){
            node.right = add(node.right,key,value);
        }else {
            node.value = value;
        }
        //更新每个节点的高度
        node.height = 1+ Math.max(getHeight(node.left),getHeight(node.right));

        //计算平衡因子
        int balanceFactor = getBalanceFactor(node);

        //平衡维护
        //LL
        if(balanceFactor > 1 && getBalanceFactor(node.left) >= 0){
            return rightRotate(node);
        }else if(balanceFactor < -1 && getBalanceFactor(node.right) <= 0){
            //RR
            return leftRotate(node);
        }else if(balanceFactor > 1 && getBalanceFactor(node.left) < 0){
            //LR(先左旋转一次：变成了LL，再右旋转)
            node.left = leftRotate(node.left);
            return rightRotate(node);
        }else if(balanceFactor < -1 && getBalanceFactor(node.right) > 0){
            //RL(先右旋转一次：变成RR，再左旋转)
            node.right = rightRotate(node.right);
            return leftRotate(node);
        }

        return node;
    }

    //从二分搜索树中删除元素为（K,V）的节点
    public V remove(K key){
        Node node = getNode(root, key);
        if(node != null){
            root = remove(root,key);
            return node.value;
        }

        return null;

    }

    //返回以node为根的二分搜索树的最小值所在的节点
    private Node minimum(Node node){
        if(node.left == null){
            return node;
        }

        return minimum(node.left);
    }

    //删除掉以node为根的二分搜索树中的最小节点
    //返回删除节点后新的二分搜索树的根
    private Node removeMin(Node node){
        if(node.left == null){
            Node rightNode = node.right;
            node.right = null;
            size --;
            return rightNode;
        }

        node.left = removeMin(node.left);
        return node;
    }


    //删除以node为根的二分搜索树中键为key的节点，递归算法
    //返回删除节点后新的二分搜索树的根
    private Node remove(Node node , K key){
        //最基本的情况：没找到键为key的节点
        if(node == null){
            return null;
        }

        Node retNode;
        if(key.compareTo(node.key) < 0){
            node.left = remove(node.left, key);
            retNode = node;
        }else if(key.compareTo(node.key) > 0){
            node.right = remove(node.right,key);
            retNode = node;
        }else{
            if(node.left == null){
                Node rightNode = node.right;
                node.right = null;
                size --;
                retNode = rightNode;
            }else if(node.right == null){
                Node leftNode = node.left;
                node.left = null;
                size --;
                retNode = leftNode;
            }else {
                //node的左右孩子都不为空
                Node successor = minimum(node.right);
                successor.right = remove(node.right,successor.key);
                successor.left = node.left;

                node.left = node.right = null;
                retNode = successor;
            }
        }

        if(retNode == null){
            return null;
        }

        //更新每个节点的高度
        retNode.height = 1+ Math.max(getHeight(retNode.left),getHeight(retNode.right));

        //计算平衡因子
        int balanceFactor = getBalanceFactor(retNode);

        //平衡维护
        //LL
        if(balanceFactor > 1 && getBalanceFactor(retNode.left) >= 0){
            return rightRotate(retNode);
        }else if(balanceFactor < -1 && getBalanceFactor(retNode.right) <= 0){
            //RR
            return leftRotate(retNode);
        }else if(balanceFactor > 1 && getBalanceFactor(retNode.left) < 0){
            //LR(先左旋转一次：变成了LL，再右旋转)
            retNode.left = leftRotate(retNode.left);
            return rightRotate(retNode);
        }else if(balanceFactor < -1 && getBalanceFactor(retNode.right) > 0){
            //RL(先右旋转一次：变成RR，再左旋转)
            retNode.right = rightRotate(retNode.right);
            return leftRotate(retNode);
        }
        return retNode;
    }


    public boolean contains(K key) {
        Node node = getNode(root, key);
        return node == null ? false : true;
    }


    public V get(K key) {
        Node node = getNode(root, key);
        if(node == null){
            throw new IllegalArgumentException("There is no such element in BST");
        }
        return node.value;
    }


    public void set(K key, V newValue) {
        Node node = getNode(root, key);
        if(node == null){
            return;
        }
        node.value = newValue;
    }

    //判断是否仍是二分搜索树
    public boolean isBST(){
        ArrayList<K> keys = new ArrayList<>();
        inOrder(root,keys);

        for (int i=1;i<keys.size();i++){
            if(keys.get(i-1).compareTo(keys.get(i)) > 0){
                return false;
            }
        }
        return true;
    }

    //二分搜索树的中序遍历
    private void inOrder(Node node,ArrayList<K> keys){
        if(node == null){
            return;
        }

        inOrder(node.left,keys);
        keys.add(node.key);
        inOrder(node.right,keys);

    }

    //判断是否仍是平衡二叉树（任意节点的左子树与右子树高度相差小于等于1）
    public boolean isBalanced(){
        return isBalanced(root);
    }

    private boolean isBalanced(Node node){
        if(node == null){
            return true;
        }

        int balancedFactor = getBalanceFactor(node);
        //Math.abs(balancedFactor) 取balancedFactor的绝对值
        if(Math.abs(balancedFactor) > 1){
            return false;
        }
        return isBalanced(node.left) && isBalanced(node.right);

    }

    public int getSize() {
        return size;
    }


    public boolean isEmpty() {
        return size == 0;
    }

    public static void main(String[] args){

        System.out.println("Pride and Prejudice");

        ArrayList<String> words = new ArrayList<>();
        if(FileOperation.readFile("pride-and-prejudice.txt", words)) {
            System.out.println("Total words: " + words.size());

            AVLTree<String, Integer> map = new AVLTree<>();
            for (String word : words) {
                if (map.contains(word))
                    map.set(word, map.get(word) + 1);
                else
                    map.add(word, 1);
            }
//            System.out.println(map.remove("pride"));
            System.out.println("Total different words: " + map.getSize());
            System.out.println("Frequency of PRIDE: " + map.get("pride"));
            System.out.println("Frequency of PREJUDICE: " + map.get("prejudice"));
            System.out.println("is BST: " + map.isBST());
            System.out.println("is Balanced: " + map.isBalanced());

            for (String word : words){
                map.remove(word);
                if(!map.isBST() || !map.isBalanced()){
                    throw new IllegalArgumentException("Error");
                }
            }
        }

        System.out.println();
    }
}
