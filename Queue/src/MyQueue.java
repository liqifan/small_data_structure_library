import java.util.Stack;

/**
 * 用栈来实现队列
 */
class MyQueue {
    
    private int size;
    private Stack<Integer> stack;
    
    /** Initialize your data structure here. */
    public MyQueue() {
        stack = new Stack<>();
        size = 0;
    }
    
    /** Push element x to the back of queue. */
    public void push(int x) {
        stack.push(x);
        size ++;
    }
    
    /** Removes the element from in front of queue and returns that element. */
    public int pop() {
        int ret = stack.get(0);
        stack.remove(0);
        size --;
        return ret;
    }
    
    /** Get the front element. */
    public int peek() {
        if(empty()){
            throw new IllegalArgumentException("peek Faild,Queue is empty");
        }
        return stack.get(0);
    }
    
    /** Returns whether the queue is empty. */
    public boolean empty() {
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        MyQueue obj = new MyQueue();
        obj.push(1);
        obj.push(2);
        int param_2 = obj.pop();
        int param_3 = obj.peek();
        boolean param_4 = obj.empty();
        System.out.println(param_2+".."+param_3+".."+param_4);
    }
}

/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue obj = new MyQueue();
 * obj.push(x);
 * int param_2 = obj.pop();
 * int param_3 = obj.peek();
 * boolean param_4 = obj.empty();
 */

