/**
 * 自定义队列
 * @param <T>
 */
public class ArrayQueue<T> implements Queue<T> {

    Array<T> array;

    public ArrayQueue(int capacity){
        array = new Array<>(capacity);
    }

    public ArrayQueue(){
        array = new Array<>();
    }

    /**
     * 向队列中添加元素
     * @param t
     */
    @Override
    public void enqueue(T t) {
        array.addLast(t);
    }

    /**
     * 从队列中取出元素
     * @return
     */
    @Override
    public T dequeue() {
        return array.removeFirst();
    }

    /**
     * 查看队首元素值
     * @return
     */
    @Override
    public T getFront() {
        return array.getFirst();
    }

    @Override
    public int getSize() {
        return array.getSize();
    }

    @Override
    public boolean isEmpty() {
        return array.isEmpty();
    }

    /**
     * 查看容量
     * @return
     */
    public int getCapacity(){
        return array.getCapacity();
    }

    @Override
    public String toString(){
        StringBuilder res = new StringBuilder();
        res.append("Queue: ");
        res.append("front [");
        for(int i=0;i<array.getSize();i++){
            res.append(array.get(i));
            if(i != array.getSize() -1){
                res.append(", ");
            }
        }
        res.append("] tail");
        return res.toString();
    }
}
