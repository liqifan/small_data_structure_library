import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

class Solution3 {
    public static List<String> letterCasePermutation(String S) {

        TreeSet<String> set = new TreeSet<>();
        List<String> list = new ArrayList<>();

        if(set.isEmpty()){
            set.add(S.toLowerCase());
        }
        for(int i=0;i<S.length();i++){
            StringBuilder res = new StringBuilder();
            if(S.charAt(i) >='a' && S.charAt(i) <='z'){
                res.append(S.substring(0,i));
                res.append(S.substring(i,i+1).toUpperCase());
                res.append(S.substring(i+1));
                set.add(res.toString());
            }else if(S.charAt(i) >='A' && S.charAt(i) <='Z'){
                res.append(S.substring(0,i));
                res.append(S.substring(i,i+1).toLowerCase());
                res.append(S.substring(i+1));
                set.add(res.toString());
            }
            System.out.println(res);
        }
        set.add(S.toUpperCase());
        for(int i=set.size();i>0;i--){
            list.add(set.pollLast());
        }
        return list;
    }

    public static void main(String[] args) {
        String s= "Jw";
        List<String> list = letterCasePermutation(s);
        System.out.println(list);
    }
}