public class LinkedListSet<T> implements Set<T> {

    private LinkedList<T> linkedList;

    public LinkedListSet(){
        linkedList = new LinkedList<>();
    }

    @Override
    public void add(T t) {
        if(!contains(t)){
            linkedList.addFirst(t);
        }
    }

    @Override
    public void remove(T t) {
        linkedList.removeElement(t);
    }

    @Override
    public boolean contains(T t) {
        return linkedList.contains(t);
    }

    @Override
    public boolean isEmpty() {
        return linkedList.isEmpty();
    }

    @Override
    public int getSize() {
        return linkedList.getSize();
    }
}
