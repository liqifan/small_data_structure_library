public interface Set<T> {

    void add(T t);
    void remove(T t);
    boolean contains(T t);
    boolean isEmpty();
    int getSize();
}
