/**
 * 基于 AVL树 实现的集合Set
 * @param <T>
 */
public class AVLSet<T extends Comparable<T>> implements Set<T> {

    private AVLTree<T,Object> avl;

    public AVLSet(){
        avl = new AVLTree<>();
    }

    @Override
    public void add(T t) {
        avl.add(t,null);
    }

    @Override
    public void remove(T t) {
        avl.remove(t);
    }

    @Override
    public boolean contains(T t) {
        return avl.contains(t);
    }

    @Override
    public boolean isEmpty() {
        return avl.isEmpty();
    }

    @Override
    public int getSize() {
        return avl.getSize();
    }
}
