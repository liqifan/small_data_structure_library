/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */

/**
 * 删除指定的节点
 *      思路：完全copy要删除节点的下一个节点
 */
class Solution3 {

    public class ListNode {
        int val;
        ListNode next;
        ListNode(int x) { val = x; }
    }

    public void deleteNode(ListNode node) {
         node.val = node.next.val;
         node.next = node.next.next;
    }
}