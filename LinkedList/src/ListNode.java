public class ListNode{
        public int val;
        public ListNode next;

        public ListNode(int val,ListNode next){
            this.val = val;
            this.next = next;
        }

        public ListNode(int val){
            this.val = val;
            this.next = null;
        }

        public ListNode(){
            this(0,null);
        }

        //使用arr为参数，创建一个链表，当前的listNode为链表头节点
        public ListNode(int[] arr){
            if(arr == null || arr.length <=0){
                throw  new IllegalArgumentException("arr is empty");
            }

//            this.val = arr[0];
            ListNode cur = this;
            cur.val = arr[0];
            for(int i=1;i<arr.length;i++){
                cur.next = new ListNode(arr[i]);
                cur = cur.next;
            }
        }

        @Override
    public String toString(){
            StringBuilder res = new StringBuilder();
            ListNode cur = this;
            res.append("head: "+ cur.val+"\n");
            while (cur != null){
                res.append(cur.val+"->");
                cur = cur.next;
            }
            res.append("NULL");
            return res.toString();
        }
    }