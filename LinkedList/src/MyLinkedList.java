class MyLinkedList {

    private class ListNode{
        private int val;
        private ListNode next;

        public ListNode(int val,ListNode next){
            this.val = val;
            this.next = next;
        }

        private ListNode(int val){
            this.val = val;
            this.next = null;
        }

        public ListNode(){
            this(0,null);
        }
    }

    private ListNode dummyHead;
    private int size;

    /** Initialize your data structure here. */
    public MyLinkedList() {
        dummyHead = new ListNode();
        size = 0;
    }

    /** Get the value of the index-th node in the linked list. If the index is invalid, return -1. */
    public int get(int index) {
        if(index < 0 || index >= size){
            return -1;
        }
        ListNode prev = dummyHead;
        for(int i=0;i<index;i++){
            prev = prev.next;
        }
        return prev.next.val;
    }

    /** Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list. */
    public void addAtHead(int val) {
        addAtIndex(0,val);
    }

    /** Append a node of value val to the last element of the linked list. */
    public void addAtTail(int val) {
        ListNode addNode = new ListNode(val);
        ListNode prev = dummyHead;
        for(int i=0;i<size;i++){
            prev = prev.next;
        }
        addNode.next = prev.next;
        prev.next = addNode;
        size ++;
    }

    /** Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted. */
    public void addAtIndex(int index, int val) {
        if(index > size){
            return;
        }
        if(index < 0){
            addAtIndex(0,val);
            return;
        }
        if(index == size && size != 0){
            addAtTail(val);
            return;
        }
        ListNode addNode = new ListNode(val);
        ListNode prev = dummyHead;
        for(int i=0;i<index;i++){
            prev = prev.next;
        }
        addNode.next = prev.next;
        prev.next = addNode;
        size ++;
    }

    /** Delete the index-th node in the linked list, if the index is valid. */
    public void deleteAtIndex(int index) {
        if(index <0 || index >= size){
            return;
        }
        ListNode prev = dummyHead;
        for(int i=0;i<index;i++){
            prev = prev.next;
        }
        ListNode delNode = prev.next;
        prev.next =delNode.next;
        size --;
    }
}

/**
 * Your MyLinkedList object will be instantiated and called as such:
 * MyLinkedList obj = new MyLinkedList();
 * int param_1 = obj.get(index);
 * obj.addAtHead(val);
 * obj.addAtTail(val);
 * obj.addAtIndex(index,val);
 * obj.deleteAtIndex(index);
 */