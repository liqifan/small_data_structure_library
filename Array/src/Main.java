public class Main {

    public static void main(String[] args) {
        Array<Integer> arr = new Array<>();
        for(int i =0;i<10;i++){
            arr.addLast(i);
        }
        System.out.println(arr);

        arr.add(1,2000);
        arr.addFirst(-20);
        System.out.println(arr);

        System.out.println(arr.get(2));
        arr.set(11,3);
        System.out.println(arr);

        System.out.println(arr.find(3));
        System.out.println(arr);
        System.out.println(arr.findAll(3));
        System.out.println(arr);
        System.out.println(arr.remove(5));
        System.out.println(arr);
        System.out.println(arr.removeElement(3));
        System.out.println(arr);
//        System.out.println(arr.remove2Element(3));



    }
}
