public class Student {
    private String name;
    private int scores;

    public Student(String name, int scores) {
        this.name = name;
        this.scores = scores;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", scores=" + scores +
                '}';
    }

    public static void main(String[] args) {
        Array<Student> array = new Array<>();

        array.addLast(new Student("小明",100));
        array.addLast(new Student("小红",66));
        array.addLast(new Student("小强",80));

        System.out.println(array);
    }
}
